//
//  DocumentViewController.h
//  DrawingApp
//
//  Created by Nikolay Chaban on 5/12/16.
//  Copyright © 2016 Nikolay Chaban. All rights reserved.
//

#import "MFDocumentViewController.h"

@interface DocumentViewController : MFDocumentViewController

@end
