//
//  SmoothDrawingView.m
//  DrawingApp
//
//  Created by Nikolay Chaban on 5/10/16.
//  Copyright © 2016 Nikolay Chaban. All rights reserved.
//

#import "DrawingView.h"

typedef NS_ENUM(NSUInteger, DrawState)
{
    DrawStateType,
    EraseStateType,
    UndoStateType,
};

@interface DrawingView()

// properties
@property (nonatomic, strong) UIBezierPath* path;
@property (nonatomic, strong) UIImage* incrementalImage;
@property (nonatomic, assign) uint ctr;
@property (nonatomic, strong) UIColor* drawingColor;
@property (nonatomic, assign) DrawState drawState;
@property (nonatomic, strong) NSMutableArray* drawPathsArray;

// methods

- (void) setupDefaults;

- (void) drawBitmap;

@end

@implementation DrawingView
{
   CGPoint pts[5]; // we now need to keep track of the four points of a Bezier segment and the first control point of the next segment
}

#pragma mark - Initialization -

- (instancetype) initWithCoder: (NSCoder*) aDecoder
{
    if ( self = [super initWithCoder: aDecoder] )
    {
        [self setupDefaults];
    }
    
    return self;
}

- (id) initWithFrame: (CGRect) frame
{
    if ( self = [super initWithFrame: frame] )
    {
        [self setupDefaults];
    }
    
    return self;
}

#pragma mark - Defaults -

- (void) setupDefaults
{
    self.path                 = [UIBezierPath bezierPath];
    self.drawingColor         = [UIColor blackColor];
    self.multipleTouchEnabled = NO;
    self.path.lineWidth       = 2.0;
    self.drawState            = DrawStateType;
    self.drawPathsArray       = [NSMutableArray new];
}

#pragma mark - Memory managment -

- (void) dealloc
{
    [self.path removeAllPoints];
    [self.drawPathsArray removeAllObjects];
    
    self.ctr              = 0;
    self.incrementalImage = nil;
}


#pragma mark - Drawing -

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void) drawRect: (CGRect) rect
{
    [self.incrementalImage drawInRect: rect];
    
    [self drawingPath];
}


#pragma mark - Public methods -

- (void) setDrawingColor: (UIColor*) color
{
    _drawingColor = color;
}

- (void) clearAllDraws
{
    [self.path removeAllPoints];
    [self.drawPathsArray removeAllObjects];
    
    [self setNeedsDisplay];
    
    self.ctr              = 0;
    self.incrementalImage = nil;
    
    [self drawBitmap];
}

- (void) enableEraser
{
    self.drawState      = EraseStateType;
    self.path.lineWidth = 10.0f;
}

- (void) disableEraser
{
    self.drawState      = DrawStateType;
    self.path.lineWidth = 2.0f;
}

- (void) undoLastPath
{
    self.drawState         = UndoStateType;
     self.incrementalImage = nil;
    
    [self.drawPathsArray removeLastObject];
    
    [self drawBitmap];
    
    [self setNeedsDisplay];
}

- (NSArray*) getAllPaths
{
    return [self.drawPathsArray copy];
}


#pragma mark - Touches handling -

- (void) touchesBegan: (NSSet*)   touches
            withEvent: (UIEvent*) event
{
    self.ctr = 0;
    
    UITouch* touch = [touches anyObject];
    
    pts[0] = [touch locationInView: self];
}

- (void) touchesMoved: (NSSet*)   touches
            withEvent: (UIEvent*) event
{
    UITouch* touch = [touches anyObject];
    
    CGPoint p = [touch locationInView: self];
    
    self.ctr++;
    
    pts[self.ctr] = p;
    
    if (self.ctr == 4)
    {
        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0); // move the endpoint to the middle of the line joining the second control point of the first Bezier segment and the first control point of the second Bezier segment
        
        [self.path moveToPoint: pts[0]];
        [self.path addCurveToPoint: pts[3]
                     controlPoint1: pts[1]
                     controlPoint2: pts[2]]; // add a cubic Bezier from pt[0] to pt[3], with control points pt[1] and pt[2]
        
        [self setNeedsDisplay];
        
        // replace points and get ready to handle the next segment
        pts[0] = pts[3];
        pts[1] = pts[4];
        
        self.ctr = 1;
    }
    
}

- (void) touchesEnded: (NSSet*)   touches
            withEvent: (UIEvent*) event
{
    [self drawBitmap];
    
    [self.drawPathsArray addObject: @{@"PathKey"      : [self.path copy],
                                      @"DrawStateKey" : @(self.drawState),
                                      @"ColorKey"     : self.drawingColor}];
    
    [self setNeedsDisplay];
    [self.path removeAllPoints];
    
    self.ctr = 0;
}

- (void) touchesCancelled: (NSSet*)   touches
                withEvent: (UIEvent*) event
{
    [self touchesEnded: touches
             withEvent: event];
}

#pragma mark - Internal methods -

- (void) drawBitmap
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
    
    if (!self.incrementalImage) // first time; paint background white
    {
        UIBezierPath* rectpath = [UIBezierPath bezierPathWithRect: self.bounds];
        
        [[UIColor clearColor] setFill];
        
        [rectpath fill];
    }
    
    [self.incrementalImage drawAtPoint: CGPointZero];
    
    [self drawingPath];
    
    self.incrementalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
}

- (void) drawingPath
{
    switch (self.drawState)
    {
        case DrawStateType:
        case EraseStateType:
        {
            [self.drawingColor setStroke];
            
            [self.path strokeWithBlendMode: (self.drawState == DrawStateType) ? kCGBlendModeNormal : kCGBlendModeClear
                                     alpha: 1.0f];
        }
            break;
        case UndoStateType:
        {
            [self.drawPathsArray enumerateObjectsUsingBlock: ^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop) {
                
                UIBezierPath* path     = obj[@"PathKey"];
                DrawState tmpDrawState = [obj[@"DrawStateKey"] integerValue];
                UIColor* tmpDrawColor  = obj[@"ColorKey"];
                
                [tmpDrawColor setStroke];
                
                [path strokeWithBlendMode: (tmpDrawState == DrawStateType) ? kCGBlendModeNormal : kCGBlendModeClear
                                    alpha: 1.0f];
                
            }];
        }
            break;
        default:
            break;
    }
}

@end
