//
//  ViewController.m
//  DrawingApp
//
//  Created by Nikolay Chaban on 5/10/16.
//  Copyright © 2016 Nikolay Chaban. All rights reserved.
//

#import "ViewController.h"
#import "DrawingView.h"
#import "MFDocumentManager.h"
#import "DocumentViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet SmoothDrawingView *drawingView;
@property (nonatomic, strong) MFDocumentManager* docManager;
@property (nonatomic, strong) DocumentViewController* documentController;

- (IBAction) onSaveBtn: (UIBarButtonItem*) sender;


- (IBAction) onBluePenBtn: (UIBarButtonItem*) sender;

- (IBAction) onGreePenBtn: (UIBarButtonItem*) sender;

- (IBAction) onClearAllBtn: (UIBarButtonItem*) sender;

- (IBAction) onEnablingLasticBtn: (UIBarButtonItem*) sender;

- (IBAction) onUndoBtn: (UIBarButtonItem*) sender;

@end

@implementation ViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString* filePath = [[NSBundle mainBundle] pathForResource: @"Lesson - 01 - Setup the Wi-Fi connection" ofType: @"pdf"];
    NSURL* fileURL     = [NSURL fileURLWithPath: filePath];
    
    self.docManager         = [[MFDocumentManager alloc] initWithFileUrl: fileURL];
    self.documentController = [[DocumentViewController alloc] initWithDocumentManager: self.docManager];
    
    [self.documentController setScrollEnabled: NO];
    
    [self addChildViewController: self.documentController];
    [self.view addSubview: self.documentController.view];
    [self.documentController didMoveToParentViewController: self];
    
    [self.view insertSubview: self.drawingView
                aboveSubview: self.documentController.view];
}

#pragma mark - Memory managment -

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions -

- (IBAction) onSaveBtn: (UIBarButtonItem*) sender
{
    
}

- (IBAction) onBluePenBtn: (UIBarButtonItem*) sender
{
    [self.drawingView setDrawingColor: [UIColor blueColor]];
    
    [self.drawingView disableEraser];
}

- (IBAction) onGreePenBtn: (UIBarButtonItem*) sender
{
    [self.drawingView setDrawingColor: [UIColor greenColor]];
    
    [self.drawingView disableEraser];
}

- (IBAction) onClearAllBtn: (UIBarButtonItem*) sender
{
    [self.drawingView clearAllDraws];
    
    [self.drawingView disableEraser];
}

- (IBAction) onEnablingLasticBtn: (UIBarButtonItem*) sender
{
    [self.drawingView enableEraser];
}

- (IBAction) onUndoBtn: (UIBarButtonItem*) sender
{
    [self.drawingView undoLastPath];
}

@end
