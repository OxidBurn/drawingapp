//
//  SmoothDrawingView.h
//  DrawingApp
//
//  Created by Nikolay Chaban on 5/10/16.
//  Copyright © 2016 Nikolay Chaban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawingView : UIView

// properties


// methods

- (void) setDrawingColor: (UIColor*) color;

- (void) clearAllDraws;

- (void) enableEraser;

- (void) disableEraser;

- (void) undoLastPath;

- (NSArray*) getAllPaths;

@end
