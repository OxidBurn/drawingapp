//
//  LinearInterpView.m
//  DrawingApp
//
//  Created by Nikolay Chaban on 5/10/16.
//  Copyright © 2016 Nikolay Chaban. All rights reserved.
//

#import "LinearInterpView.h"

@implementation LinearInterpView
{
    UIBezierPath* path;
}

#pragma mark - Initialization -

- (instancetype) initWithCoder: (NSCoder*) aDecoder
{
    if ( self = [super initWithCoder: aDecoder] )
    {
        [self setMultipleTouchEnabled: NO];
        [self setBackgroundColor: [UIColor whiteColor]];
        
        path = [UIBezierPath bezierPath];
        
        [path setLineWidth: 2.0];
    }
    
    return self;
}

#pragma mark - Drawing -

- (void) drawRect: (CGRect) rect
{
    [[UIColor blackColor] setStroke];
    
    [path stroke];
}


#pragma mark - Touches -

- (void) touchesBegan: (NSSet*)   touches
            withEvent: (UIEvent*) event
{
    UITouch* touch = [touches anyObject];
    CGPoint p      = [touch locationInView: self];
    
    [path moveToPoint: p];
}

- (void) touchesMoved: (NSSet*)   touches
            withEvent: (UIEvent*) event
{
    UITouch* touch = [touches anyObject];
    CGPoint p      = [touch locationInView:self];
    
    [path addLineToPoint: p];
    
    [self setNeedsDisplay];
}

- (void) touchesEnded: (NSSet*)   touches
            withEvent: (UIEvent*) event
{
    [self touchesMoved: touches
             withEvent: event];
}

- (void) touchesCancelled: (NSSet*)   touches
                withEvent: (UIEvent*) event
{
    [self touchesEnded: touches
             withEvent: event];
}

@end
